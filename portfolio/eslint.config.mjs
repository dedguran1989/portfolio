// eslint.config.mjs
export default {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    "next", // Используйте базовую конфигурацию Next.js, если она подходит вашему проекту
    "eslint:recommended", // Базовые рекомендуемые правила ESLint
    "plugin:react/recommended", // Рекомендуемые правила для React
    "prettier", // Отключает правила ESLint, которые могут конфликтовать с Prettier
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: "module",
  },
  plugins: [
    "react", // Этот плагин нужен, если вы используете React
    "prettier", // Добавляет плагин prettier для запуска Prettier как правила ESLint
  ],
  rules: {
    "prettier/prettier": "error", // Показывает ошибки форматирования как ошибки ESLint
    // Здесь вы можете добавить или переопределить дополнительные правила
  },
  settings: {
    react: {
      version: "detect", // Автоматически обнаруживает версию React для использования плагином eslint-plugin-react
    },
  },
};
