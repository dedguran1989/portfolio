import Link from "next/link";
import React from "react";
import {
  RiLinkedinFill,
  RiGitlabFill,
  RiFacebookFill,
  RiTelegramFill,
} from "react-icons/ri";

const icons = [
  {
    path: "https://www.linkedin.com/in/andrey-gurlev-b95b6b185/",
    name: <RiLinkedinFill />,
  },
  {
    path: "https://gitlab.com/DedGuran",
    name: <RiGitlabFill />,
  },
  {
    path: "https://www.facebook.com/dedguran",
    name: <RiFacebookFill />,
  },
  {
    path: "https://t.me/dedguran",
    name: <RiTelegramFill />,
  },
];

const Socials = ({ containerStyles, iconsStyles }) => {
  return (
    <div className={`${containerStyles}`}>
      {icons.map((icon, index) => {
        return (
          <Link href={icon.path} key={index}>
            <div className={`${iconsStyles}`}>{icon.name}</div>
          </Link>
        );
      })}
    </div>
  );
};

export default Socials;
