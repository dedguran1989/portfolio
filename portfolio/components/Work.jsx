"use client";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";

import { Button } from "./ui/button";
import { Pagination } from "swiper/modules";
import ProjectCard from "./ProjectCard";
import Link from "next/link";
const projectData = [
  {
    image: "/work/1.png",
    category: "Yii2",
    name: "Taishettoday Website",
    description:
      "Small town blog where information about events and history of the city is published",
    link: "https://taishettoday.ru/",
    github: "https://github.com/GuranAmbal/TaishetToday",
  },
  {
    image: "/work/2.png",
    category: "Fullstack",
    name: "VedaInfo Website",
    description:
      "Blog with information about events in the field of self-development",
    link: "",
    github: "https://gitlab.com/DedGuran/blog-next.js",
  },
  {
    image: "/work/3.png",
    category: "react native",
    name: "Listen Lectures App",
    description: "Application for listening to lectures",
    link: "",
    github: "https://gitlab.com/DedGuran/appmobilevedarn",
  },
  {
    image: "/work/4.png",
    category: "Remix",
    name: "Shopify app",
    description:
      "Application for adding an information block about Influencer to pages with a specific parameter",
    link: "",
    github: "https://gitlab.com/DedGuran/flopti",
  },
];

const Work = () => {
  return (
    <section className="relative mb-12 xl:mb-48">
      <div className="container mx-auto">
        {/*text */}
        <div
          className="max-w-[350px] flex-col mx-auto 2xl:mx-0 text-center 
              2xl:text-left mb-12 2xl:h-[400px] flex 
              justify-center items-center 2xl:items-start"
        >
          <h2 className="section-title mb-4">Latest Projects</h2>
          <p className="subtitle mb-8">
            My projects fuse advanced technology with creative solutions,
            addressing key industry challenges with user-focused applications
            and streamlined backend operations. Through innovation and a
            dedication to excellence, I strive to both empower users and inspire
            the market.
          </p>
          <Link href="/projects">
            <Button className="gap-x-2">All Projects</Button>
          </Link>
        </div>
        {/*slider */}
        <div className="2xl:max-w-[1000px]  2xl:absolute right-0 top-0">
          <Swiper
            className="h-[480px]"
            slidesPerView={1}
            breaakpoints={{
              640: {
                slidesPerView: 2,
              },
            }}
            spaceBetween={20}
            modules={[Pagination]}
            pagination={{ clickable: true }}
          >
            {/*4 projects for slides */}
            {projectData.slice(0, 4).map((project, index) => {
              return (
                <SwiperSlide key={index}>
                  <ProjectCard project={project} />
                </SwiperSlide>
              );
            })}
          </Swiper>
        </div>
      </div>
    </section>
  );
};

export default Work;
