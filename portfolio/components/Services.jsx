import { GanttChartSquare, Blocks, Gem } from "lucide-react";
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "./ui/card";

const servicesData = [
  {
    icon: <GanttChartSquare size={72} strokeWidth={0.8} />,
    title: "DevOps Development",
    description:
      "I am proficient in CI/CD with GitLab, use Docker. With expertise in Linux server administration on AWS and version control with Git, I efficiently manage server migrations and configurations for nginx and Apache2.",
  },
  {
    icon: <Blocks size={72} strokeWidth={0.8} />,
    title: "Web Development",
    description:
      "I'm proficient in full-stack web development, skilled in front-end technologies (HTML, CSS, JavaScript, React, remix) and back-end (Node.js, PHP, MySQL, MongoDB, PostgreSQL), with a focus on SEO and accessibility. I aim for clean, scalable code.",
  },
  {
    icon: <Gem size={72} strokeWidth={0.8} />,
    title: "App Development",
    description:
      "I specialize in React Native for app development, focusing on cross-platform iOS and Android applications. Skilled in backend integration, push notifications, and database management with Firebase and SQLite.",
  },
];

const Services = () => {
  return (
    <section className="mb-12 xl:mb-36">
      <div className="container mx-auto">
        <h2 className="section-title mb-12 xl:mb-24 text-center mx-auto">
          My Services
        </h2>
        {/*grid items*/}
        <div className="grid 2xl:grid-cols-3 justify-center gap-y-12 2xl:gap-y-24 2xl:gap-x-8">
          {servicesData.map((item, index) => {
            return (
              <Card
                className="w-full max-w-[424px] h-[300px] flex flex-col pt-24 pb-10 justify-center items-center relative"
                key={index}
              >
                <CardHeader className="text-primary absolute -top-[60px]">
                  <div className="w-[140px] h-[70px] bg-white dark:bg-background flex justify-center items-center">
                    {item.icon}
                  </div>
                </CardHeader>
                <CardContent className="text-center">
                  <CardTitle className="mb-4">{item.title}</CardTitle>
                  <CardDescription className="text-lg">
                    {item.description}
                  </CardDescription>
                </CardContent>
              </Card>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Services;
