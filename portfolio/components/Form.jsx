import React from "react";
import { User, MailIcon, ArrowRightIcon, MessageSquare } from "lucide-react";
import { Input } from "./ui/input";
import { Textarea } from "./ui/textarea";
import { Button } from "./ui/button";

const Form = () => {
  return (
    <form
      action="https://formspree.io/f/meqypzpd"
      method="POST"
      className="flex flex-col gap-y-4"
    >
      {/*input */}
      <div className="relative flex items-center">
        <Input type="text" id="name" name="name" placeholder="Name" />
        <User className="absolute right-6" size={20} />
      </div>
      <div className="relative flex items-center">
        <Input type="email" id="email" name="email" placeholder="Email" />
        <User className="absolute right-6" size={20} />
      </div>
      <div className="relative flex items-center">
        <Textarea
          id="message"
          name="message"
          placeholder="Type Your Message Here."
        />
        <MessageSquare className="absolute top-4 right-6" size={20} />
      </div>
      <Button type="submit" className="flex items-center max-w-[166px] gap-x-1">
        Let's Talk
        <ArrowRightIcon size={20} />
      </Button>
    </form>
  );
};

export default Form;
