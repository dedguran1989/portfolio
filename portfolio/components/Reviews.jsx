"use client";

import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";

import { Button } from "./ui/button";
import { Card, CardDescription, CardHeader, CardTitle } from "./ui/card";
import { Pagination } from "swiper/modules";

const reviewsData = [
  {
    avatar: "/reviews/avatar-1.png",
    name: "Gena Qarchava",
    job: "Senior Account Manager",
    review: `I am writing to express my full support for Andrey Gurlev's application for the position of
    Frontend Developer at your esteemed company. As the Chief Operating Officer at
    InsightWhale, I have had the pleasure of witnessing Andrey's exceptional growth and
    contributions to our business over the years. He possesses a rare combination of
    dedication, innovation, and communication skills, making him an invaluable asset to any
    team.
    Throughout his tenure, Andrey has consistently demonstrated a high level of
    competence in understanding project requirements, effectively communicating with
    both developers and our management team, and delivering timely, high-quality
    solutions. His ability to articulate complex technical concepts in an understandable
    manner has significantly enhanced the productivity and cohesion of our team.
    Moreover, Andrey's proactive approach and perfectionism in problem-solving, coupled
    with his dedication to continuous learning and improvement, have greatly contributed to
    the success of our projects. He is not only skilled in programming but also excels in
    creating a positive and collaborative work environment.
    I am confident that Andrey will bring the same level of excellence, dedication, and
    innovation to your team and make a significant contribution to the success of your
    projects.`,
  },
  {
    avatar: "/reviews/avatar-4.jpeg",
    name: "Anton Voronov",
    job: "CEO, Founder",
    review: `Reliability, responsibility, constant desire for new knowledge - Andrey has a number of skills and good habits that allow him to confidently move along the path of development, benefiting his colleagues, the company and clients, which is a good basis for his stable career growth.
    Andrey knows how to solve complex technical problems (including in emergency situations): professionally, calmly, quickly understands the causes and eliminates them.
    Andrey is a team player, representative of a culture of respect, care, gratitude and goodwill.
    Andrey is an employee who is devoted to the company, goes through its ups and downs, helps his colleagues, and creates a healthy psychological atmosphere around himself.
    I recommend Andrey as an expert in the field of software development, as a team player and a promising employee.`,
  },
  // {
  //   avatar: "/reviews/avatar-2.png",
  //   name: "Viacheslav Matyushin",
  //   job: "Senior CRO Expert",
  //   review: `Andrey has consistently demonstrated high levels of competence in
  //   understanding the project requirements, effective communication with both developers,
  //   as well as with the management team of our team, and timely delivery
  //   quality solutions. His ability to clearly communicate complex technical
  //   concept has clearly significantly increased productivity and
  //   the cohesion of our team.
  //   In addition, Andrey’s proactive approach and perfectionism to solving problems and
  //   commitment to continuous learning and improvement significantly
  //   contributed to the success of our projects. He is not only skilled in
  //   programming, but also succeeds in creating positive and
  //   collaborative work environment.`,
  // },
  // {
  //   avatar: "/reviews/avatar-3.png",
  //   name: "Vasily Kolesov",
  //   job: "Senior Frontend Developer",
  //   review: `Andrey has consistently demonstrated high levels of competence in
  //   understanding the project requirements, effective communication with both developers,
  //   as well as with the management team of our team, and timely delivery
  //   quality solutions. His ability to clearly communicate complex technical
  //   concept has clearly significantly increased productivity and
  //   the cohesion of our team.
  //   In addition, Andrey’s proactive approach and perfectionism to solving problems and
  //   commitment to continuous learning and improvement significantly
  //   contributed to the success of our projects. He is not only skilled in
  //   programming, but also succeeds in creating positive and
  //   collaborative work environment.`,
  // },
];

const Reviews = () => {
  return (
    <section className="mb-12 xl:mb-32">
      <div className="container mx-auto">
        <h2 className="section-title mb-12 text-center mx-auto">Reviews</h2>
        {/*slider */}
        <Swiper>
          {reviewsData.map((person, index) => {
            return (
              <SwiperSlide
                slidesPerView={1}
                breakpoints={{
                  640: { slidesPerView: 2 },
                  1400: { slidesPerView: 3 },
                }}
                spaceBetween={30}
                modules={[Pagination]}
                pagination={{
                  clickable: true,
                }}
                className="h-[350px]"
                key={index}
              >
                <Card className="bg-tertiary dark:bg-secondary/40 p-8 min-h-[300px]">
                  <CardHeader className="p-0 mb-10">
                    <div className="flex items-center gap-x-4">
                      {/*image */}
                      <Image
                        className="rounded-full"
                        src={person.avatar}
                        width={70}
                        height={70}
                        alt=""
                        priority
                        rounded
                      />
                      {/*name */}
                      <div>
                        <CardTitle>{person.name}</CardTitle>
                        <p>{person.job}</p>
                      </div>
                    </div>
                  </CardHeader>
                  <CardDescription className="text-lg text-muted-foreground">
                    {person.review}
                  </CardDescription>
                </Card>
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
    </section>
  );
};

export default Reviews;
