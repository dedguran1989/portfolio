import Form from "@/components/Form";
import { MailIcon, HomeIcon, PhoneCall } from "lucide-react";

function Contact() {
  return (
    <section>
      <div className="container x-auto">
        {/*text & illustration */}
        <div className="grid xl:grid-cols-2 pt-12 xl:h-[480px] mb-6 xl:mb-24">
          {/*text */}
          <div className="flex flex-col justify-center">
            {/* <div className="flex items-center gap-x-4 text-primary text-lg mb-4">
              <span className="w-[30px] h-[2px] bg-primary"></span>
              ✋Say Hello
            </div> */}
            <h1 className="h1 max-w-md mb-8">Let's Work Together</h1>
            <p>
              I always enjoy hearing from you! If you have a question, a project
              idea, or simply want to share your thoughts, please feel free to
              get in touch. Your insights and ideas are invaluable to me as they
              help me grow and improve. Here’s how you can reach out:
            </p>
          </div>
          {/*illustration */}
          <div className="hidden xl:flex w-full bg-contact_illustration_light dark:contact_illustration_dark bg-contain bg-top bg-no-repeat"></div>
        </div>
        {/*info text &  form*/}
        <div className="grid xl:grid-cols-2 mb-24 xl:mb-32">
          {/*info text */}
          <div className="flex flex-col gap-y-4 xl:gap-y-14 mb-12 xl:mb-24 text-base xl:text-lg">
            {/*mail */}
            <div className="flex items-center gap-x-8">
              <MailIcon size={18} className="text-primary" />
              <div>dedguran@gmail.com</div>
            </div>
            {/*address */}
            <div className="flex items-center gap-x-8">
              <HomeIcon size={18} className="text-primary" />
              <div>67 Mayasovsky str., Tayiset, Irkutsk region</div>
            </div>
            {/*mail */}
            <div className="flex items-center gap-x-8">
              <PhoneCall size={18} className="text-primary" />
              <div>+7 964 26 73 180</div>
            </div>
          </div>
          <Form />
        </div>
      </div>
    </section>
  );
}

export default Contact;
