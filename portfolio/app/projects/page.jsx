"use client";
import ProjectCard from "@/components/ProjectCard";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { useState } from "react";

const projectData = [
  {
    image: "/work/1.png",
    category: "Yii2",
    name: "Taishettoday Website",
    description:
      "Small town blog where information about events and history of the city is published",
    link: "https://taishettoday.ru/",
    github: "https://github.com/GuranAmbal/TaishetToday",
  },
  {
    image: "/work/2.png",
    category: "Fullstack",
    name: "VedaInfo Website",
    description:
      "Blog with information about events in the field of self-development",
    link: "",
    github: "https://gitlab.com/DedGuran/blog-next.js",
  },
  {
    image: "/work/3.png",
    category: "react native",
    name: "Listen Lectures App",
    description: "Application for listening to lectures",
    link: "",
    github: "https://gitlab.com/DedGuran/appmobilevedarn",
  },
  {
    image: "/work/4.png",
    category: "Remix",
    name: "Shopify app",
    description:
      "Application for adding an information block about Influencer to pages with a specific parameter",
    link: "https://getfanfuel.com/",
    github: "https://gitlab.com/DedGuran/flopti",
  },
];

//remove category duplicate

const uniqueCategories = [
  "all projects",
  ...new Set(projectData.map((item) => item.category)),
];
function Projects() {
  const [categories, setCategories] = useState(uniqueCategories);
  const [category, setCategory] = useState("all projects");

  const filteredProjects = projectData.filter((project) => {
    return category === "all projects"
      ? project
      : project.category === category;
  });
  return (
    <section className="min-h-screen pt-12">
      <div className="container mx-auto">
        <h2 className="section-title mb-8 xl:mb-16 text-center mx-auto">
          My Projects
        </h2>
        {/*tabs */}
        <Tabs defaultValue={category} className="mb-24 xl:mb-48">
          <TabsList
            className="w-full grid h-full md:grid-cols-5 lg:max-w-[640px] mb-12 
          mx-auto md:border dark:border-none"
          >
            {categories.map((category, index) => {
              return (
                <TabsTrigger
                  onClick={() => setCategory(category)}
                  value={category}
                  key={index}
                  className="capitalize w-[162px] md:w-auto"
                >
                  {category}
                </TabsTrigger>
              );
            })}
          </TabsList>
          {/*tabs content */}
          <div className="text-lg xl:mt-8 grid grid-cols-1 lg:grid-cols-3 gap-4">
            {filteredProjects.map((project, index) => {
              return (
                <TabsContent value={category} key={index}>
                  <ProjectCard project={project} />
                </TabsContent>
              );
            })}
          </div>
        </Tabs>
      </div>
    </section>
  );
}

export default Projects;
