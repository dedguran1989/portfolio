module.exports = {
  apps: [
    {
      script: "npm start",
    },
  ],

  deploy: {
    production: {
      key: "portfolioKey.pem",
      user: "ubuntu",
      host: "34.230.81.221",
      ref: "origin/main",
      repo: "https://gitlab.com/dedguran1989/portfolio.git",
      path: "/home/ubuntu",
      "pre-deploy-local": "",
      "post-deploy":
        "cd /home/ubuntu/source/portfolio && source ~/.nvm/nvm.sh && npm install &&npm run build && pm2 reload ecosystem.config.js --env production",
      "pre-setup": "",
      ssh_options: "ForwardAgent=yes",
    },
  },
};
